__author__ = 'judithe'

import random


def choose_from_hist():
    t = ['a', 'a', 'b']
    hist = {}
    for item in t:
        hist[item] = hist.get(item, 0) + 1

    list_ = []
    for key in hist:
        for i in range(0, hist[key]):
            list_.append(key)
    return random.choice(list_)


def main():
    a = 0
    b = 0
    for i in range(0, 10000):
        if choose_from_hist() == 'a':
            a += 1
        else:
            b += 1
    print("a: %.5f" % (a / 10000.0), "b: %.5f" % (b / 10000.0))

main()
