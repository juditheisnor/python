__author__ = 'judithe'



import string


def process_file(filename, skip_header):
    """
     histogramme qui contient les mots du fichier.

    """
    hist = {}
    fp = open(filename)

    if skip_header:
        skip_gutenberg_header(fp)

    for line in fp:
        process_line(line, hist)
    return hist


def skip_gutenberg_header(fp):
    """Reads from fp until it finds the line that ends the header.

    fp: open file object
    """
    for line in fp:
        if line.startswith('*** START OF THIS PROJECT'):
            break


def process_line(line, hist):
    """Adds the words in the line to the histogram.

    Modifies hist.

    line: string
    hist: histogram (map from word to frequency)
    """
    # replace hyphens with spaces before splitting
    line = line.replace('-', ' ')

    for word in line.split():
        # remove punctuation and convert to lowercase
        word = word.strip(string.punctuation + string.whitespace)
        word = word.lower()

        # update the histogram
        hist[word] = hist.get(word, 0) + 1



if __name__ == '__main__':
    hist = process_file('ThePrince.txt', skip_header=True)

    words = process_file('words.txt', skip_header=False)

    word_find = set(hist) - set(words)
    print("The words in the book that aren't in the word list are:")
    for word in word_find:
        print(word)